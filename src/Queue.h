#include <memory>
#include <vector>
#include <stdlib.h>
#include <atomic>

#ifndef QUEUE_H
#define QUEUE_H

class Task;

const std::size_t queue_size = (1 << 23);

class Queue
{
<<<<<<< HEAD
    public:
        /** Default constructor */
        Queue() {
            this->top = 0;
            this->bottom = 0;
            this->logSize = CA_SIZE;
            this->data = (Task **) malloc(LOG_SIZE(CA_SIZE) * sizeof(Task *));
        }

        #define enqueue(queue,task) {               \
            setPos(queue,queue.bottom,task);        \
            barrier();                              \
            queue.bottom++;                         \
        }

        //inline void enqueue(Task * task) {
        //  setPos((*this),this->bottom,task);
            //Sem esta barreora de memoria fica-se em livelock. Mas com ela fica +-30% mais lento...
        //    barrier();
        //    ++this->bottom;
        //}

        /*#define tryDequeue(queue, task) {                                   \
            long int b = --queue.bottom;                                    \
            long int t = queue.top;                                         \
            long int size = b - t;                                          \
            if(b - t < 0) {                                                 \
                queue.bottom = t;                                           \
            } else {                                                        \
                task = getPos(queue,b);                                     \
                if(!size) {                                                 \
                    if(!__sync_bool_compare_and_swap(&(queue.top),t,t+1)) { \
                        task = nullptr;                                     \
                    }                                                       \
                    queue.bottom = t+1;                                     \
                }                                                           \
                                                                            \
            }                                                               \
        }*/



        inline Task * tryDequeue() {
            --this->bottom;
            long int b = this->bottom;
            long int t = this->top;
            long int size = b - t;
            if(size < 0) {
                bottom = t;
                return nullptr;
            }
            Task * task = getPos((*this),b);
            if(size > 0) {
                return task;
            }

            if(!__sync_bool_compare_and_swap(&top,t,t+1)) {
                task = nullptr;
            }
            this->bottom = t+1;
            return task;
        }

        /*#define trySteal(queue, task) {                                 \
            long int t = queue.top;                                     \
            long int b = queue.bottom;                                  \
            long int size = b - t;                                      \
            if(size > 0) {                                              \
                task = getPos(queue,t);                                 \
                if(!__sync_bool_compare_and_swap(&(queue.top),t,t+1))   \
                    task = nullptr;                                     \
            }                                                           \
        }*/


        #define trySteal(queue,task) {                                          \
            long int __t = queue.top;                                           \
            if(queue.bottom - __t > 0) {                                        \
                task = getPos(queue,__t);                                       \
                if(!__sync_bool_compare_and_swap(&(queue.top),__t,__t+1))       \
                    task = nullptr;                                             \
            }                                                                   \
        }


        /*inline Task * trySteal() {
            long int t = this->top;
            if(this->bottom - t <= 0) return nullptr;
            Task * d = getPos((*this),t);
            if(!__sync_bool_compare_and_swap(&top,t,t+1)) return nullptr;
            return d;
        }*/

        /** Default destructor */
        virtual ~Queue() {
            free(data);
        }

        long int top;
        long int bottom;
        Task ** data;
        int logSize;

    protected:
    private:

=======
    class circular_array {
	public:
		circular_array(std::size_t n) {
            items = (std::atomic<Task*>*) malloc(sizeof(std::atomic<Task*>) * n);
            length = n;
		}

		~circular_array() {
            free(items);
		}

		std::size_t size() {
            return length;
		}

		Task* get(std::size_t index)
		{
			return items[index & (length - 1)].load(std::memory_order_relaxed);
		}
		void put(std::size_t index, Task* x)
		{
			items[index & (length - 1)].store(x, std::memory_order_relaxed);
		}
	private:
	    std::size_t length;
		std::atomic<Task*>* items;
		std::unique_ptr<circular_array> previous;
	};

    std::atomic<circular_array*> array;
	std::atomic<std::size_t> top, bottom;




public:
    /** Default constructor */
    Queue(): top(0), bottom(0), array(new circular_array(queue_size)) {}

    inline void enqueue(Task * data) {
        std::size_t b = bottom.load(std::memory_order_relaxed);
		std::size_t t = top.load(std::memory_order_acquire);
		circular_array* a = array.load(std::memory_order_relaxed);
		a->put(b, data);
		std::atomic_thread_fence(std::memory_order_release);
		bottom.store(b + 1, std::memory_order_relaxed);
    }

    inline Task * tryDequeue() {
        std::size_t b = bottom.load(std::memory_order_relaxed) - 1;
		circular_array* a = array.load(std::memory_order_relaxed);
		bottom.store(b, std::memory_order_relaxed);
		std::atomic_thread_fence(std::memory_order_seq_cst);
		std::size_t t = top.load(std::memory_order_relaxed);
		if (t <= b) {
			Task* x = a->get(b);
			if (t == b) {
				if (!top.compare_exchange_strong(t, t + 1, std::memory_order_seq_cst, std::memory_order_relaxed))
					x = nullptr;
				bottom.store(b + 1, std::memory_order_relaxed);
			}
			return x;
		} else {
			bottom.store(b + 1, std::memory_order_relaxed);
			return nullptr;
		}
    }


    inline Task * trySteal() {
        std::size_t t = top.load(std::memory_order_acquire);
		std::atomic_thread_fence(std::memory_order_seq_cst);
		std::size_t b = bottom.load(std::memory_order_acquire);
		Task* x = nullptr;
		if (t < b) {
			circular_array* a = array.load(std::memory_order_relaxed);
			x = a->get(t);
			if (!top.compare_exchange_strong(t, t + 1, std::memory_order_seq_cst, std::memory_order_relaxed))
				return nullptr;
		}
		return x;
    }

    /** Default destructor */
    virtual ~Queue() {
        circular_array* p = array.load(std::memory_order_relaxed);
		if (p)
			delete p;
    }
>>>>>>> 4e97d7b439161ffbb0706e56f70c7d3d820fa58a
};

#endif // QUEUE_H
