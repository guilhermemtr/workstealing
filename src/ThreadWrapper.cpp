#include "ThreadWrapper.h"
#include "ThreadPool.h"
#include "DataTask.h"

ThreadWrapper::ThreadWrapper(Scheduler * sched, int wIndex) {
    generator = new RandomNumberGenerator();
    this->sched = sched;
    stop = false;
    workerId = wIndex;
}

ThreadWrapper::~ThreadWrapper() {
    delete this->generator;
}

void ThreadWrapper::work() {
    Task * task = nullptr;
    while(true){
        while(task == nullptr){
            if(stop) return;
            task = sched->getScheduledTask(workerId, generator);
        }

        task->call(this);
        task = nullptr;

    }
}
