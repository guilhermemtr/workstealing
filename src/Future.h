#include "RandomNumberGenerator.h"
#include "ThreadWrapper.h"
#include <memory>

/**
 * Waits for the future completion.
 * This is an abstraction to the get__ call.
 */
#define get() get__(this)

#ifndef FUTURE_H
#define FUTURE_H

#include <pthread.h>

class Future
{
    friend class Task;

    public:

        /**
         * Default constructor
         */
        inline Future() {
            pthread_mutex_init(&(this->mutex),NULL);
            pthread_cond_init(&(this->done), NULL);
            this->ended = false;
            this->broadcasted = false;
        }


        /**
         * Default destructor
         */
        virtual inline ~Future() {
            pthread_mutex_lock(&(this->mutex));
            while(!this->broadcasted) {
                pthread_mutex_unlock(&(this->mutex));
                pthread_mutex_lock(&(this->mutex));
            }
            pthread_mutex_unlock(&(this->mutex));
            pthread_cond_destroy(&(this->done));
            pthread_mutex_destroy(&(this->mutex));
        }

        /**
         * Blocks waiting for the future completion.
         */
        virtual inline void wait() {
            pthread_mutex_lock(&(this->mutex));
            while(!this->ended)
                pthread_cond_wait(&(this->done), &(this->mutex));
            pthread_mutex_unlock(&(this->mutex));
        }

        /**
         * Waits for the future completion.
         * @param task <code>Task *</code> the task in which get was called.
         * The task can and is hidden by <code>#define get() get__(self())</code>
         */
        virtual void get__(Task * task);

        /**
         * If the future was already reached.
         * @return <code>bool</code> whether the future was reached.
         */
        virtual inline bool reached() {
            return this->ended;
        }

        /**
         * Marks the future as reached.
         */
        virtual inline void end() {
            this->ended = true;
            pthread_mutex_lock(&(this->mutex));
            pthread_cond_broadcast(&(this->done));
            this->broadcasted = true;
            pthread_mutex_unlock(&(this->mutex));
        }

    protected:

        //If the future was reached or not.
        bool ended;

        //Indicates if the future was already broadcasted.
        bool broadcasted;

        //The mutex.
        pthread_mutex_t mutex;
        //The completion condition.
        pthread_cond_t done;

    private:
};

#endif // FUTURE_H
