#include "ThreadPool.h"
#include "Task.h"

//Each thread's stack size
#define STACK_SIZE 1024*1024*32

void * thread_work(void * thread_wrapper) {
    ThreadWrapper * thr = (ThreadWrapper *) thread_wrapper;
    thr->work();
    pthread_exit(NULL);
}

void ThreadPool::init(int numWorkers, Task * initialTask)
{
    this->numWorkers = numWorkers;
    this->initialTask = initialTask;
    prng = new RandomNumberGenerator();
    sched = new Scheduler();
    sched->scheduleTask(0, initialTask);
    threads = (ThreadWrapper **) malloc(sizeof(ThreadWrapper *) * numWorkers);
    pthreads = (pthread_t *) malloc(sizeof(pthread_t) * numWorkers);
    for(int i = 0; i < numWorkers; i++) {
        threads[i] = new ThreadWrapper(sched, i);
    }
    pthread_attr_t attributes;
    pthread_attr_init(&attributes);
    pthread_attr_setstacksize(&attributes,STACK_SIZE);
    pthread_attr_setscope(&attributes, PTHREAD_SCOPE_SYSTEM);
    for(int i = 0; i < numWorkers; i++) {
        pthread_create(&(pthreads[i]),&attributes,thread_work,threads[i]);
    }
}

ThreadPool::ThreadPool(Task * initialTask)
{
    init(get_nprocs(),initialTask);
}

ThreadPool::ThreadPool(int numWorkers, Task * initialTask)
{
    init(numWorkers,initialTask);
}

Future * ThreadPool::getFuture(void)
{
    return initialTask->getFuture();
}

int ThreadPool::getNumWorkers(void)
{
    return numWorkers;
}

ThreadPool::~ThreadPool()
{
    for(int i = 0; i < numWorkers; i++){
        threads[i]->stopWorker();
        pthread_join(pthreads[i],NULL);
        delete threads[i];
    }
    delete this->prng;
    delete this->sched;
    free(this->pthreads);
    free(this->threads);
    this->initialTask = nullptr;
    this->sched = nullptr;
}
