#include "RandomNumberGenerator.h"
#include "Scheduler.h"

#ifndef THREADWRAPPER_H
#define THREADWRAPPER_H

class ThreadPool;

class ThreadWrapper
{
    friend class Future;
    friend class Task;
    public:

        /**
         * Default constructor.
         * @param sched <code>Scheduler *</code> the scheduler.
         * @param id <code>long int</code> the worker id.
         */
        ThreadWrapper(Scheduler * sched, int id);

        /**
         * Default destructor
         */
        virtual ~ThreadWrapper();

        /**
         * The threads work function.
         */
        virtual void work(void);

        /**
         * Sets the flag stop to true.
         * This makes the worker stop trying to get new work,
         * and exiting.
         */
        virtual void stopWorker(void) {
            this->stop = true;
        }

        //Flag that represents whether the worker can keep trying to fetch
        // and execute tasks or if it should stop and exit
        volatile bool stop;

    protected:
        //The workers random number generator.
        RandomNumberGenerator * generator;

        //The workers scheduler.
        Scheduler * sched;

    private:

        //The worker id.
        int workerId;
};

#endif // THREADWRAPPER_H
