#ifndef ASSERTEDDATATASK_H
#define ASSERTEDDATATASK_H

#include "DataTask.h"
#include <assert.h>

class AssertedDataTask: public DataTask
{
    public:

        /**
         * Default constructor.
         * @param parallelSize <code>long int</code> the parallel size.
         */
        AssertedDataTask(long int parallelSize):DataTask(parallelSize){}

        /**
         * Calls the task.
         */
        void call(ThreadWrapper * thr) {
            this->setWorker(thr);
            this->run();
            if(this->current >= this->parallelSize) {
                assert(this->assertion());
                this->future->end();
            }
        }

        /**
         * Asserts the task executed correctly.
         * @return <code>bool</code> whether the assertion was respected or not.
         */
        virtual bool assertion(void) = 0;

        /**
         * Default destructor
         */
        virtual ~AssertedDataTask(void) {};

    protected:
    private:
};

#endif // ASSERTEDDATATASK_H
