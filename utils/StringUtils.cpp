#include "StringUtils.h"
//I got this code from web.
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> getLimits(std::string &str, char separator)
{
    std::vector<std::string> elems;
    split(str, separator, elems);
    return elems;
}


